db.rooms.insertMany([
    {
        name:"Double",
        accomodates:3,
        price:2000,
        description:"A room fit for a small family going on a vacation",
        rooms_available:5,
        isAvailable:false
    },
    {
        name:"Queen",
        accomodates:4,
        price:4000,
        description:"A room with a queen sized bed perfect for a simple getaway",
        rooms_available:15,
        isAvailable:false
    }
])